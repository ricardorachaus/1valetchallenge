# 1Valet Challenge

## Requirements

- Xcode 13+

## About

The project was written in **Swift 5** and **Xcode 13**. The minimum target used was **iOS 13.0**. The project used **UIKit** with AutoLayout **programatically**. The architeture used was **MVP with Coordinators**.

The tests are divided in **Unit Tests** and **UI Tests**. The Unit tests has the focus in testing all the main paths and classes of the code. All the classes were tested with **solitary tests** using **testing doubles like stubs and spies**. The UI tests contains only two that tests the more used behaviors.

**No external dependencies** were used.

### Author: Ricardo Rachaus