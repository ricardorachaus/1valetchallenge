import Foundation

@testable import _ValetChallenge

final class URLSessionDataTaskDummy: URLSessionDataTask {
    override func resume() {}
}

final class URLSessionStub: URLSession {
    
    var error: NetworkError?
    var returnNoData: Bool = false
    
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        if let error = error {
            completionHandler(nil, nil, error)
        } else if returnNoData {
            completionHandler(nil, nil, nil)
        } else {
            let encoded = try? JSONEncoder().encode(DeviceList.dummy())
            completionHandler(encoded, nil, nil)
        }
        return URLSessionDataTaskDummy()
    }
    
}
