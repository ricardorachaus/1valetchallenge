import Foundation

final class UserDefaultsSpy: UserDefaults {
    
    var hasSaved: Bool = false
    var hasRetrieved: Bool = false
    
    override func set(_ value: Any?, forKey defaultName: String) {
        hasSaved = true
    }
    
    override func object(forKey defaultName: String) -> Any? {
        hasRetrieved = true
        return ["1234"]
    }

}
