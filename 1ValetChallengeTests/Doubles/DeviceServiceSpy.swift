@testable import _ValetChallenge

final class DeviceServiceSpy: DeviceServiceProtocol {
    
    var hasCalledDevices: Bool = false
    var hasCalledSave: Bool = false
    var hasCalledRetrieve: Bool = false
    
    func devices(completion: @escaping (Result<[Device], NetworkError>) -> Void) {
        hasCalledDevices = true
        completion(.success([]))
    }
    
    func save(deviceID: String) {
        hasCalledSave = true
    }
    
    func retrieveDevicesIDs() -> [String] {
        hasCalledRetrieve = true
        return []
    }
    
}
