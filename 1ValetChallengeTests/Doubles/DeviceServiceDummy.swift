import Foundation
@testable import _ValetChallenge

final class DeviceServiceDummy: DeviceServiceProtocol {
    func devices(completion: @escaping (Result<[Device], NetworkError>) -> Void) {}
    func save(deviceID: String) {}
    func retrieveDevicesIDs() -> [String] { [] }
}
