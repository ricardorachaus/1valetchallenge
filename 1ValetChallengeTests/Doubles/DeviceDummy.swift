@testable import _ValetChallenge

extension Device {
    static func dummy() -> Device {
        return Device(id: "1", type: .lights, price: 0, currency: "", isFavorite: false, imageUrl: "", title: "Some Lights", description: "")
    }
}

extension DeviceList {
    static func dummy() -> DeviceList {
        return DeviceList(devices: [.dummy()])
    }
}
