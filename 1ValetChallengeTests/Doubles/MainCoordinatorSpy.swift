@testable import _ValetChallenge

final class MainCoordinatorSpy: MainCoordinatorDelegate {
    
    var hasCalledHome: Bool = false
    var hasCalledMyDevices: Bool = false
    var hasCalledDetails: Bool = false
    
    func presentHome() {
        hasCalledHome = true
    }
    
    func presentMyDevices() {
        hasCalledMyDevices = true
    }
    
    func presentDetails(for device: Device) {
        hasCalledDetails = true
    }

}
