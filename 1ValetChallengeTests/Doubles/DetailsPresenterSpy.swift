@testable import _ValetChallenge

final class DetailsPresenterSpy: DetailsPresenterProtocol {
    
    var hasCalledFavorite: Bool = false
    var hasCalledIsFavorite: Bool = false
    
    func favorite(device: Device) {
        hasCalledFavorite = true
    }
    
    func isFavorite(device: Device) -> Bool {
        hasCalledIsFavorite = true
        return true
    }
    
}
