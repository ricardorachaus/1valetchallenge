@testable import _ValetChallenge

final class ListPresenterSpy: ListPresenterProtocol {
    
    var hasLoadedData: Bool = false
    var hasPresentedDevice: Bool = false
    
    func loadData(onSuccess: @escaping ([Device]) -> ()) {
        hasLoadedData = true
        onSuccess([.dummy(), .dummy()])
    }
    
    func present(device: Device) {
        hasPresentedDevice = true
    }

}
