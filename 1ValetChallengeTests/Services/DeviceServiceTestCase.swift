import XCTest

@testable import _ValetChallenge

final class DeviceServiceTestCase: XCTestCase {
    
    var sut: DeviceService!
    var sessionStub: URLSessionStub!
    var defaultsSpy: UserDefaultsSpy!
    
    override func setUp() {
        super.setUp()
        sessionStub = URLSessionStub()
        defaultsSpy = UserDefaultsSpy()
        sut = DeviceService(session: sessionStub, defaults: defaultsSpy)
    }
    
    override func tearDown() {
        sut = nil
        defaultsSpy = nil
        sessionStub = nil
        super.tearDown()
    }
    
    func testGetDevicesSuccess() {
        sut.devices { result in
            let devices = try? result.get()
            XCTAssertEqual(devices?.count, 1)
            XCTAssertEqual(devices?.first, .dummy())
        }
    }
    
    func testGetDevicesError() {
        sessionStub.error = .unknown
        
        sut.devices { result in
            if case let .failure(error) = result {
                XCTAssertEqual(error, .unknown)
            } else {
                XCTFail("Did not get error")
            }
        }
    }
    
    func testGetDevicesNoData() {
        sessionStub.returnNoData = true
        
        sut.devices { result in
            if case let .failure(error) = result {
                XCTAssertEqual(error, .invalidData)
            } else {
                XCTFail("Did not get error")
            }
        }
    }
    
    func testRetrieveIds() {
        let result = sut.retrieveDevicesIDs()
        
        XCTAssertEqual(result.count, 1)
        XCTAssertTrue(defaultsSpy.hasRetrieved)
    }
    
    func testSaveDevice() {
        sut.save(deviceID: "123")
        
        XCTAssertTrue(defaultsSpy.hasSaved)
    }
    
}
