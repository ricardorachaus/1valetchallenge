import XCTest
@testable import _ValetChallenge

final class ListViewControllerTestCase: XCTestCase {
    
    var sut: ListViewController!
    var presenterSpy: ListPresenterSpy!
    
    override func setUp() {
        super.setUp()
        
        presenterSpy = ListPresenterSpy()
        sut = ListViewController(title: "Home", presenter: presenterSpy)
        
        sut.beginAppearanceTransition(true, animated: false)
        sut.endAppearanceTransition()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testConfigureViewController() {
        let tableView = UITableView()
        tableView.register(ListViewCell.self)
        tableView.dataSource = sut

        let numberOfRows = sut.tableView(tableView, numberOfRowsInSection: 0)
        let height = sut.tableView(tableView, heightForRowAt: IndexPath())
        let cell = sut.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? ListViewCell
        
        XCTAssertTrue(presenterSpy.hasLoadedData)
        XCTAssertEqual(numberOfRows, 2)
        XCTAssertEqual(height, 56)
        XCTAssertEqual(cell?.nameLabel.text, "Some Lights")
        XCTAssertEqual(cell?.typeLabel.text, "Type: Lights")
    }
    
    func testPresentDeviceDetails() {
        sut.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(presenterSpy.hasPresentedDevice)
    }
    
    func testSearchBarSearchButtonClicked() {
        sut.searchBarSearchButtonClicked(UISearchBar())
        
        XCTAssertTrue(sut.hasSearched)
        XCTAssertEqual(sut.filteredDevices.count, 0)
        XCTAssertEqual(sut.devices.count, 2)
    }
    
    func testSearchBarCancelButtonClicked() {
        sut.searchBarCancelButtonClicked(UISearchBar())
        
        XCTAssertFalse(sut.hasSearched)
        XCTAssertEqual(sut.filteredDevices.count, 0)
        XCTAssertEqual(sut.devices.count, 2)
    }
    
    func testSearchBarTextDidChange() {
        sut.searchBar(UISearchBar(), textDidChange: "")
        
        XCTAssertTrue(sut.hasSearched)
        XCTAssertEqual(sut.filteredDevices.count, 0)
        XCTAssertEqual(sut.devices.count, 2)
    }
    
}
