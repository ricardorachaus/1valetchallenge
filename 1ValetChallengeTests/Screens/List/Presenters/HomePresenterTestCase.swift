import XCTest
@testable import _ValetChallenge

final class HomePresenterTestCase: XCTestCase {
    
    var sut: HomePresenter!
    var serviceSpy: DeviceServiceSpy!
    var coordinatorSpy: MainCoordinatorSpy!
    
    override func setUp() {
        super.setUp()
        coordinatorSpy = MainCoordinatorSpy()
        serviceSpy = DeviceServiceSpy()
        
        sut = HomePresenter(coordinator: coordinatorSpy, service: serviceSpy)
    }
    
    override func tearDown() {
        sut = nil
        serviceSpy = nil
        coordinatorSpy = nil
        super.tearDown()
    }
    
    func testLoadData() {
        sut.loadData { devices in
            XCTAssertEqual(devices.count, 0)
            XCTAssertTrue(self.serviceSpy.hasCalledDevices)
        }
    }
    
    func testPresentDetails() {
        sut.present(device: .dummy())
        
        XCTAssertTrue(coordinatorSpy.hasCalledDetails)
    }
    
}
