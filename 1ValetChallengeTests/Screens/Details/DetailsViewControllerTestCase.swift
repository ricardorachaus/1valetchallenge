import XCTest
@testable import _ValetChallenge

final class DetailsViewControllerTestCase: XCTestCase {
    
    var sut: DetailsViewController!
    var presenterSpy: DetailsPresenterSpy!
    
    override func setUp() {
        super.setUp()
        presenterSpy = DetailsPresenterSpy()
        sut = DetailsViewController(device: .dummy(), presenter: presenterSpy)
        
        sut.beginAppearanceTransition(true, animated: false)
        sut.endAppearanceTransition()
    }
    
    override func tearDown() {
        sut = nil
        presenterSpy = nil
        super.tearDown()
    }
    
    func testConfigureViewController() {
        XCTAssertEqual(sut.title, "Some Lights")
        XCTAssertTrue(presenterSpy.hasCalledIsFavorite)
    }
    
    func testFavoriteDevice() {
        sut.favorite()
        
        XCTAssertTrue(presenterSpy.hasCalledFavorite)
    }
    
}
