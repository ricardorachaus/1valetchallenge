import XCTest
@testable import _ValetChallenge

final class DetailsPresenterTestCase: XCTestCase {
    
    var sut: DetailsPresenter!
    var serviceSpy: DeviceServiceSpy!
    
    override func setUp() {
        super.setUp()
        serviceSpy = DeviceServiceSpy()
        sut = DetailsPresenter(service: serviceSpy)
    }
    
    override func tearDown() {
        sut = nil
        serviceSpy = nil
        super.tearDown()
    }
    
    func testFavoriteDevice() {
        sut.favorite(device: .dummy())
        
        XCTAssertTrue(serviceSpy.hasCalledSave)
    }
    
    func testIsFavoriteDevice() {
        let result = sut.isFavorite(device: .dummy())
        
        XCTAssertFalse(result)
        XCTAssertTrue(serviceSpy.hasCalledRetrieve)
    }
    
}
