import XCTest

@testable import _ValetChallenge

final class MainCoordiantorTestCase: XCTestCase {
    
    var sut: MainCoordinator!
    var tabBarControllerSpy: UITabBarController!
    
    override func setUp() {
        super.setUp()
        tabBarControllerSpy = UITabBarController()
        sut = MainCoordinator(rootViewController: tabBarControllerSpy, service: DeviceServiceDummy())
        
        sut.setup()
    }
    
    override func tearDown() {
        sut = nil
        tabBarControllerSpy = nil
        super.tearDown()
    }
    
    func testSetupCoordinator() {
        XCTAssertEqual(tabBarControllerSpy.viewControllers?.count, 2)
        XCTAssertEqual(tabBarControllerSpy.viewControllers?[0].title, "Home")
        XCTAssertEqual(tabBarControllerSpy.viewControllers?[1].title, "My Devices")
        XCTAssertEqual(tabBarControllerSpy.selectedIndex, 0)
    }
    
    func testPresentHome() {
        sut.presentHome()
        
        XCTAssertEqual(tabBarControllerSpy.selectedIndex, 0)
    }
    
    func testPresentMyDevices() {
        sut.presentMyDevices()
        
        XCTAssertEqual(tabBarControllerSpy.selectedIndex, 1)
    }
    
}
