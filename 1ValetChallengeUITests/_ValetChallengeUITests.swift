import XCTest

class ValetChallengeUITests: XCTestCase {
    
    func testFavoriteAndUnfavoriteDevice() {
        let app = XCUIApplication()
        app.activate()
        let sensorTagRadiowavesForwardCell = app.tables/*@START_MENU_TOKEN@*/.cells.containing(.image, identifier:"sensor.tag.radiowaves.forward").element/*[[".cells.containing(.staticText, identifier:\"Type: Sensor\").element",".cells.containing(.staticText, identifier:\"Test Sensor\").element",".cells.containing(.image, identifier:\"sensor.tag.radiowaves.forward\").element"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        sensorTagRadiowavesForwardCell.tap()
        
        let testSensorNavigationBar = app.navigationBars["Test Sensor"]
        let favoriteButton = testSensorNavigationBar.buttons["favorite"]
        favoriteButton.tap()
        
        let homeButton = testSensorNavigationBar.buttons["Home"]
        homeButton.tap()
        
        sensorTagRadiowavesForwardCell.tap()
        favoriteButton.tap()
        homeButton.tap()
        app.terminate()
    }
    
    func testFavoriteAndOpenMyDevicesAndUnfavorite() {
        let app = XCUIApplication()
        app.activate()
        let sensorTagRadiowavesForwardCell = app.tables/*@START_MENU_TOKEN@*/.cells.containing(.image, identifier:"sensor.tag.radiowaves.forward").element/*[[".cells.containing(.staticText, identifier:\"Type: Sensor\").element",".cells.containing(.staticText, identifier:\"Test Sensor\").element",".cells.containing(.image, identifier:\"sensor.tag.radiowaves.forward\").element"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        sensorTagRadiowavesForwardCell.tap()
        
        let testSensorNavigationBar = app.navigationBars["Test Sensor"]
        let favoriteButton = testSensorNavigationBar.buttons["favorite"]
        favoriteButton.tap()
        
        let homeButton = testSensorNavigationBar.buttons["Home"]
        homeButton.tap()
        
        let tabBar = app.tabBars["Tab Bar"]
        tabBar.buttons["My Devices"].tap()
        sensorTagRadiowavesForwardCell.tap()
        favoriteButton.tap()
        
        let myDevicesButton = testSensorNavigationBar.buttons["My Devices"]
        myDevicesButton.tap()
        app.terminate()
    }
    
}
