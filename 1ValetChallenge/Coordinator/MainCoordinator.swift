import UIKit

protocol MainCoordinatorDelegate {
    func presentHome()
    func presentMyDevices()
    func presentDetails(for device: Device)
}

final class MainCoordinator: Coordinator {
    
    let rootViewController: UIViewController
    
    private let service: DeviceServiceProtocol
    
    private var tabBarController: UITabBarController? {
        return rootViewController as? UITabBarController
    }
    
    init(rootViewController: UIViewController = UITabBarController(), service: DeviceServiceProtocol = DeviceService()) {
        self.rootViewController = rootViewController
        self.service = service
    }
    
    func setup() {
        let homeViewController = ListViewController(title: Constants.Strings.homeTitle, presenter: HomePresenter(coordinator: self, service: service))
        let controller = ListViewController(title: Constants.Strings.myDevicesTitle, presenter: MyDevicesPresenter(coordinator: self, service: service))
        let homeController = createNavigationController(rootViewController: homeViewController, image: .houseIcon)
        let myDevicesController = createNavigationController(rootViewController: controller, image: .phoneIcon)
        
        tabBarController?.viewControllers = [homeController, myDevicesController]
        tabBarController?.tabBar.tintColor = .label
        tabBarController?.selectedIndex = 0
    }
    
    func createNavigationController(rootViewController: UIViewController, image: UIImage?) -> UINavigationController {
        let controller = UINavigationController(rootViewController: rootViewController)
        controller.tabBarItem.title = controller.title
        controller.tabBarItem.image = image
        return controller
    }
    
}

extension MainCoordinator: MainCoordinatorDelegate {
    
    func presentHome() {
        tabBarController?.selectedIndex = 0
    }
    
    func presentMyDevices() {
        tabBarController?.selectedIndex = 1
    }
    
    func presentDetails(for device: Device) {
        let index = tabBarController?.selectedIndex ?? 0
        let navigation = tabBarController?.viewControllers?[index] as? UINavigationController
        let presenter = DetailsPresenter(service: service)
        let controller = DetailsViewController(device: device, presenter: presenter)
        navigation?.pushViewController(controller, animated: true)
    }
    
}
