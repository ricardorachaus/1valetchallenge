import UIKit

protocol Reusable: AnyObject {
    static var cellName: String { get }
}

extension Reusable {
    static var cellName: String {
        return String(describing: self)
    }
}

extension UITableViewCell: Reusable {}

extension UITableView {
    
    func register<Cell: Reusable>(_ cellType: Cell.Type) {
        register(cellType, forCellReuseIdentifier: cellType.cellName)
    }
    
    func dequeueReusableCell<Cell: Reusable>(cellType: Cell.Type, for indexPath: IndexPath) -> Cell {
        guard let cell = dequeueReusableCell(withIdentifier: cellType.cellName, for: indexPath) as? Cell else {
            fatalError("Cell of type \(String(describing: cellType)) doesn't implement Reusable protocol!")
        }
        return cell
    }
    
}
