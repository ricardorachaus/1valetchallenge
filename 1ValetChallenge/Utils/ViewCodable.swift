import Foundation

protocol ViewCodable {
    func setupView()
    func configure()
    func setupHierarchy()
    func setupConstraints()
}

extension ViewCodable {
    func setupView() {
        configure()
        setupHierarchy()
        setupConstraints()
    }
}
