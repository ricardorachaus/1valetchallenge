import Foundation

enum Constants {
    
    enum Strings {
        static let homeTitle = "Home"
        static let myDevicesTitle = "My Devices"
        static let searchBarPlaceholder = "Search Device"
        
        static func name(title: String) -> String { "Name: \(title)" }
        static func price(value: Double, currency: String) -> String { "Price: \(value) \(currency)" }
        static func type(name: String) -> String { "Type: \(name)" }
    }
    
    enum Keys {
        static let storageKey = "Devices"
    }

}
