import UIKit

extension UIImage {
    static let houseIcon = UIImage(systemName: "house")
    static let phoneIcon = UIImage(systemName: "iphone")
    static let lightsIcon = UIImage(systemName: "lightbulb")
    static let tvIcon = UIImage(systemName: "tv")
    static let thermostatIcon = UIImage(systemName: "thermometer")
    static let sensorIcon = UIImage(systemName: "sensor.tag.radiowaves.forward")
    static let infoCircleIcon = UIImage(systemName: "info.circle")
    static let starIcon = UIImage(systemName: "star")
    static let starFillIcon = UIImage(systemName: "star.fill")
}
