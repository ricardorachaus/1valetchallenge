import UIKit

struct DeviceList: Codable {
    let devices: [Device]
}

enum DeviceType: String, Codable {
    case sensor = "Sensor"
    case thermostat = "Thermostat"
    case tv = "TV"
    case lights = "Lights"
    
    var icon: UIImage? {
        switch self {
        case .sensor: return .sensorIcon
        case .thermostat: return .thermostatIcon
        case .tv: return .tvIcon
        case .lights: return .lightsIcon
        }
    }
}

struct Device {
    let id: String
    let type: DeviceType
    let price: Double
    let currency: String
    let isFavorite: Bool
    let imageUrl: String
    let title: String
    let description: String
}

extension Device: Codable, Equatable {
    func favorite() -> Device {
        return Device(
            id: id,
            type: type,
            price: price,
            currency: currency,
            isFavorite: !isFavorite,
            imageUrl: imageUrl,
            title: title,
            description: description
        )
    }
}
