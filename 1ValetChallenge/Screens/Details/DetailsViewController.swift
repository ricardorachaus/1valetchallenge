import UIKit

final class DetailsViewController: UIViewController {
    
    private let detailsView: DetailsView
    private let device: Device
    private let presenter: DetailsPresenterProtocol
    
    init(device: Device, presenter: DetailsPresenterProtocol) {
        self.detailsView = DetailsView()
        self.device = device
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = detailsView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = device.title
        detailsView.updateView(with: device)
        setRightBarButtonItem(full: presenter.isFavorite(device: device))
    }
    
    @objc func favorite() {
        presenter.favorite(device: device)
        let image: UIImage? = presenter.isFavorite(device: device) ? .starFillIcon : .starIcon
        navigationItem.rightBarButtonItem?.image = image
    }
    
    private func setRightBarButtonItem(full: Bool) {
        let image: UIImage? = presenter.isFavorite(device: device) ? .starFillIcon : .starIcon
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(favorite))
    }
    
}
