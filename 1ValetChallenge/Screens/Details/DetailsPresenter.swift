import Foundation

protocol DetailsPresenterProtocol {
    func favorite(device: Device)
    func isFavorite(device: Device) -> Bool
}

final class DetailsPresenter: DetailsPresenterProtocol {
    
    private let service: DeviceServiceProtocol
    
    init(service: DeviceServiceProtocol) {
        self.service = service
    }
    
    func favorite(device: Device) {
        service.save(deviceID: device.id)
    }
    
    func isFavorite(device: Device) -> Bool {
        return service.retrieveDevicesIDs().contains(device.id)
    }
    
}
