import UIKit

final class DetailsView: UIView {
    
    private let imageView: UIImageView
    private let titleLabel: UILabel
    private let priceLabel: UILabel
    private let typeLabel: UILabel
    
    override init(frame: CGRect = .zero) {
        imageView = UIImageView()
        titleLabel = UILabel()
        priceLabel = UILabel()
        typeLabel = UILabel()
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateView(with device: Device) {
        imageView.image = device.type.icon
        titleLabel.text = Constants.Strings.name(title: device.title)
        priceLabel.text = Constants.Strings.price(value: device.price, currency: device.currency)
        typeLabel.text =  Constants.Strings.type(name: device.type.rawValue)
    }
    
}

extension DetailsView: ViewCodable {
    
    func configure() {
        backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
    }
    
    func setupHierarchy() {
        addSubviews(imageView, titleLabel, priceLabel, typeLabel)
    }
    
    func setupConstraints() {
        imageView.constraints([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 48),
            imageView.heightAnchor.constraint(equalToConstant: 80),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, constant: 1.8)
        ])
        
        titleLabel.constraints([
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 64),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            titleLabel.heightAnchor.constraint(equalToConstant: 24)
        ])
        
        priceLabel.constraints([
            priceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16),
            priceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            priceLabel.heightAnchor.constraint(equalToConstant: 24)
        ])
        
        typeLabel.constraints([
            typeLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 16),
            typeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            typeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            typeLabel.heightAnchor.constraint(equalToConstant: 24)
        ])
    }
    
}
