import Foundation

protocol ListPresenterProtocol {
    func loadData(onSuccess: @escaping ([Device]) -> ())
    func present(device: Device)
}

final class HomePresenter: ListPresenterProtocol {
    
    private let service: DeviceServiceProtocol
    private let coordinator: MainCoordinatorDelegate
    
    init(coordinator: MainCoordinatorDelegate, service: DeviceServiceProtocol) {
        self.service = service
        self.coordinator = coordinator
    }
    
    func loadData(onSuccess: @escaping ([Device]) -> ()) {
        service.devices { result in
            switch result {
            case let .success(devices):
                onSuccess(devices)
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func present(device: Device) {
        coordinator.presentDetails(for: device)
    }
    
}
