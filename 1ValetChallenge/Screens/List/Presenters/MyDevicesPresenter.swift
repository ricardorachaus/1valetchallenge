import Foundation

final class MyDevicesPresenter: ListPresenterProtocol {
    
    private let service: DeviceServiceProtocol
    private let coordinator: MainCoordinatorDelegate
    
    init(coordinator: MainCoordinatorDelegate, service: DeviceServiceProtocol) {
        self.service = service
        self.coordinator = coordinator
    }
    
    func loadData(onSuccess: @escaping ([Device]) -> ()) {
        service.devices { result in
            switch result {
            case let .success(devices):
                let ids = self.service.retrieveDevicesIDs()
                let storedDevices = devices.filter { ids.contains($0.id) }
                onSuccess(storedDevices)
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func present(device: Device) {
        coordinator.presentDetails(for: device)
    }
    
}
