import UIKit

final class ListViewController: UIViewController {
    
    private let presenter: ListPresenterProtocol
    private let homeView: ListView
    
    private(set) var hasSearched: Bool
    private(set) var filteredDevices: [Device]
    private(set) var devices: [Device]
    
    init(title: String, presenter: ListPresenterProtocol) {
        self.homeView = ListView()
        self.presenter = presenter
        self.hasSearched = false
        self.filteredDevices = []
        self.devices = []
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = homeView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        homeView.searchBar.delegate = self
        homeView.tableView.dataSource = self
        homeView.tableView.delegate = self
        homeView.tableView.register(ListViewCell.self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.loadData { devices in
            self.devices = devices
            DispatchQueue.main.async {
                self.homeView.tableView.reloadData()
            }
        }
    }

}

extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hasSearched ? filteredDevices.count : devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(cellType: ListViewCell.self, for: indexPath)
        
        let devices = hasSearched ? filteredDevices : devices
        let device = devices[indexPath.row]
        cell.update(title: device.title, type: device.type.rawValue, icon: device.type.icon)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
}

extension ListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let devices = hasSearched ? filteredDevices : devices
        let device = devices[indexPath.row]
        presenter.present(device: device)
    }
    
}

extension ListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        updateDisplayedItems(searched: true, searchBar: searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        updateDisplayedItems(searched: false, searchBar: searchBar)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        updateDisplayedItems(searched: true, searchBar: searchBar)
    }
    
    private func updateDisplayedItems(searched: Bool, searchBar: UISearchBar) {
        hasSearched = searched
        filteredDevices = hasSearched ? devices.filter { $0.title.contains(searchBar.text ?? "") } : []
        homeView.tableView.reloadData()
    }
    
}
