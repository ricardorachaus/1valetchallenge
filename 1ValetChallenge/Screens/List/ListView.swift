import UIKit

final class ListView: UIView {
    
    private(set) var searchBar: UISearchBar
    private(set) var tableView: UITableView
    
    override init(frame: CGRect = .zero) {
        searchBar = UISearchBar()
        tableView = UITableView()
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ListView: ViewCodable {
    
    func configure() {
        backgroundColor = .white
        searchBar.showsCancelButton = true
        searchBar.placeholder = Constants.Strings.searchBarPlaceholder
    }
    
    func setupHierarchy() {
        addSubviews(searchBar, tableView)
    }
    
    func setupConstraints() {
        searchBar.constraints([
            searchBar.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: trailingAnchor),
            searchBar.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        tableView.constraints([
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
}
