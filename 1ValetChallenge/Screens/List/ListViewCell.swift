import UIKit

final class ListViewCell: UITableViewCell {
    
    let deviceImageView: UIImageView
    let nameLabel: UILabel
    let typeLabel: UILabel
    let infoImageView: UIImageView
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        deviceImageView = UIImageView()
        nameLabel = UILabel()
        typeLabel = UILabel()
        infoImageView = UIImageView()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(title: String, type: String, icon: UIImage?) {
        deviceImageView.image = icon
        nameLabel.text = title
        typeLabel.text = Constants.Strings.type(name: type)
        infoImageView.image = .infoCircleIcon
    }
    
}

extension ListViewCell: ViewCodable {
    
    func configure() {
        nameLabel.font = .systemFont(ofSize: 14, weight: .bold)
        typeLabel.font = .systemFont(ofSize: 14)
        imageView?.contentMode = .scaleAspectFit
    }
    
    func setupHierarchy() {
        addSubviews(
            deviceImageView,
            nameLabel,
            typeLabel,
            infoImageView
        )
    }
    
    func setupConstraints() {
        deviceImageView.constraints([
            deviceImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            deviceImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            deviceImageView.heightAnchor.constraint(equalToConstant: 32),
            deviceImageView.widthAnchor.constraint(equalTo: deviceImageView.heightAnchor)
        ])
        
        nameLabel.constraints([
            nameLabel.leadingAnchor.constraint(equalTo: deviceImageView.trailingAnchor, constant: 8),
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            nameLabel.heightAnchor.constraint(equalToConstant: 16)
        ])
        
        typeLabel.constraints([
            typeLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),
            typeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            typeLabel.heightAnchor.constraint(equalToConstant: 16)
        ])
        
        infoImageView.constraints([
            infoImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            infoImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            infoImageView.heightAnchor.constraint(equalToConstant: 24),
            infoImageView.widthAnchor.constraint(equalTo: infoImageView.heightAnchor)
        ])
    }
    
}
