import Foundation

enum NetworkError: Error {
    case unknown
    case invalidData
}

protocol DeviceServiceProtocol {
    func devices(completion: @escaping (Result<[Device], NetworkError>) -> Void)
    func save(deviceID: String)
    func retrieveDevicesIDs() -> [String]
}

final class DeviceService: DeviceServiceProtocol {
    
    private let session: URLSession
    private let defaults: UserDefaults
    
    init(session: URLSession = .shared, defaults: UserDefaults = .standard) {
        self.session = session
        self.defaults = defaults
    }
    
    func devices(completion: @escaping (Result<[Device], NetworkError>) -> Void) {
        guard let url = Bundle.main.url(forResource: "devices", withExtension: "json") else { return }
        
        session.dataTask(with: url) { data, response, error in
            if error != nil {
                return completion(.failure(.unknown))
            }
            
            guard let data = data else {
                return completion(.failure(.invalidData))
            }
            
            do {
                let result = try JSONDecoder().decode(DeviceList.self, from: data)
                completion(.success(result.devices))
            } catch {
                print(error.localizedDescription)
                completion(.failure(.invalidData))
            }
        }.resume()
    }
    
    func save(deviceID: String) {
        var stored = retrieveDevicesIDs()

        if stored.contains(deviceID), let index = stored.firstIndex(of: deviceID) {
            stored.remove(at: index)
            return defaults.set(stored, forKey: Constants.Keys.storageKey)
        }

        stored.append(deviceID)
        defaults.set(stored, forKey: Constants.Keys.storageKey)
    }
    
    func retrieveDevicesIDs() -> [String] {
        return defaults.object(forKey: Constants.Keys.storageKey) as? [String] ?? []
    }
    
}
